#include<stdint.h>

/* The cheksum as is defined in RFC 791 as the 16 bit 
 * one's complement of the one's complement sum of all 
 * 16 bit words in the header.  For purposes of computing 
 * the checksum, the value of the checksum field is zero.
 */
uint16_t openinj_rfc791_checksum(uint8_t *header, int header_length)
{
    uint32_t sum = 0;
    int counter = 0;
    uint8_t carry;
    uint16_t *ptr = (uint16_t *) header;

    while(counter < (header_length / 2)) {
        counter++;
        sum += *ptr;
        ptr++;
    }
    
    /* If there is a carry then it is added to the sum,
     * if that sum also has a carry, another 1 is added */
    if((sum & 0xF0000) > 0) {
        carry = (sum & 0xF0000) >> 16;
        sum &= 0xFFFF;
        sum += carry;
        if((sum & 0xFF0000) > 0)
            sum += 1;
    }

    /* One's complement */
    return ~sum;
}
