# OpenInject
libopeninject is a small passion project that aims to provide an easy interface for constructing and injecting packets in C.

## Installation

### From source
To build from source, you can run `make`:

```
$ make
$ sudo make install
```

### ArchLinux
On Arch you can install libopeninject as a package:

```
$ makepkg -si
```

### Example programs
- `garru` - An ARP cache poisoning tool
