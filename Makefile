CC=gcc
FLAGS=-I./include/ -Wall -g
ODIR = obj
LDIR = lib
SDIR = src

_OBJ = arp.o ethernet.o icmp.o interfaces.o ipv4.o ipv6.o openinject.o rfc791.o tcp.o
OBJ=$(patsubst %, $(ODIR)/%, $(_OBJ))

lib/libopeninject.so: $(OBJ)
	$(CC) $(FLAGS) -shared $(ODIR)/* -o $@

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) $(FLAGS) -c -fPIC -o $@ $<

.PHONY: clean

static: $(OBJ)
	ar rcs $(LDIR)/libopeninject.a $(ODIR)/*

check: lib/libopeninject.so
	$(CC) $(FLAGS) -L./lib test/tools/test_rfc791_checksum.c -o test/tools/test_rfc791_checksum -lopeninject -lcheck
	./test/tools/test_rfc791_checksum

install:
	cp include/* /usr/include/
	cp lib/* /usr/lib/

clean:
	rm -rf $(ODIR) $(LDIR)

$(shell   mkdir -p $(ODIR) $(LDIR))
